package prog.bcas.bank;

public class BankAccount {

		public static String bankName = " HATTON NATIONAL BANK ";
		private String branch;
		private double accountBalance = 0;
		private String accountType;
		private String accountNumber;
		private String accountHolderName;
	
		public void openAccount(String branch, String accountType, String accountNumber, String accountHolderName,
				int amount) {
			this.branch = branch;
			this.accountType = accountType;
			this.accountNumber = accountNumber;
			this.accountHolderName = accountHolderName;
			this.accountBalance = amount;
	
		}
	
		public void withdrawal(double amount) {
			accountBalance = accountBalance - amount;
	
		}
	
		public void deposit(double amount) {
			accountBalance = accountBalance + amount;
		}
	
		public void getAccountDetails(String accountNo) {
			System.out.println("*********************************************************");
			System.out.println("Bank Name  : " + bankName + "| " + branch+" |");
			System.out.println("Account Type  : " + accountType);
			System.out.println("Account Number  : " + accountNumber);
			System.out.println("Account Holder Name  : " + accountHolderName);
			System.out.println("Account Balance  : " + accountBalance);
			System.out.println("*********************************************************");
	}
	}

