package prog.bcas.bank;

public class BankDemo {

	public static void main(String[] args) {
		
				BankAccount accPrashajini = new BankAccount();
				BankAccount accPrestina = new BankAccount();
				BankAccount accVijay = new BankAccount();
				
				accPrashajini.openAccount("Colombo", "Saving Account", "10895427", "P.Prashajini", 100000);
				accPrestina.openAccount("Kandy", "Current Account", "20025930", "P.Prestina", 50000);
				accVijay.openAccount("Colombo", "Saving Account", "20108764", "P.Vijay", 200000);
				
				accPrashajini.withdrawal(20000);
				accPrestina.deposit(25000);
				accVijay.deposit(12000);
				
				accPrashajini.getAccountDetails("10895427");
				accPrestina.getAccountDetails("20025930");
				accVijay.getAccountDetails("20108764");
				
				System.out.println(accPrashajini);
				System.out.println(accPrestina);
				System.out.println(accVijay);
		
			}
		}

