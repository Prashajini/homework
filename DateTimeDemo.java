import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

public class DateTimeDemo {
		
	public static void main(String args[]) {
		System.out.println("Current Date & Time");
		
		LocalDate date = LocalDate.now();
		System.out.println(date);
		
		LocalTime time = LocalTime.now();
		System.out.println(time);
		
		LocalDateTime dateTime= LocalDateTime.now();
		System.out.println(dateTime);
		
		
		ZoneId zoneIdNY= ZoneId.of("America/New_York");
		LocalTime NYtime= LocalTime.now(zoneIdNY);
		System.out.println("New York : " + NYtime);

		ZoneId zoneIdSin= ZoneId.of("Singapore");
		LocalTime SingTime= LocalTime.now(zoneIdSin);
		System.out.println("Singapore : " + SingTime);
		
		System.out.println("Italy :" + LocalTime.now(ZoneId.of("GMT+2")));
		System.out.println("United Kingdom :" + LocalTime.now(ZoneId.of("GMT+1")));
		
	}
}
