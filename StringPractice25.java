//Java - String length() Method

public class StringPractice25 {
	
	public static void main(String args[]) {
	      String Str1 = new String("Prashajini Pratheepan");
	      String Str2 = new String("Pratheepan" );

	      System.out.print("String Length :" );
	      System.out.println(Str1.length());

	      System.out.print("String Length :" );
	      System.out.println(Str2.length());
	   }

}
