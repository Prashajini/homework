import java.util.Scanner;

public class Factorial
{
	public static void main(String args [])
	{
		int fact = 1;
		int number = 0;
		
		System.out.println("Enter a number to print its factorial");
		
		Scanner scan = new Scanner(System.in);
		number = scan.nextInt();
		
		for(int i = 1; i<= number; i++)
		{
			fact = i*fact;
		}
		System.out.println("The factorial of " + number + " is " + fact);
	}
}