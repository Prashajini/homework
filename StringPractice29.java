//Java - String replace() Method

public class StringPractice29 {
	
	public static void main(String args[]) {
	      String Str = new String("Prashajini Pratheepan");

	      System.out.print("Return Value :" );
	      System.out.println(Str.replace('a', 'T'));

	      System.out.print("Return Value :" );
	      System.out.println(Str.replace('i', 'k'));
	   }

}
